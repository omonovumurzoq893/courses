const sequelize = require("../config/db");
const { DataTypes } = require("sequelize");
const Students = require("./Students");

const Attachments = sequelize.define(
  "attachments",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    originalName: {
      type: DataTypes.STRING,
    },
    size: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    underscored: true,
  }
);

Attachments.hasOne(Students, { foreignKey: "fileId" });
Students.belongsTo(Attachments, { as: "file" });

module.exports = Attachments;
