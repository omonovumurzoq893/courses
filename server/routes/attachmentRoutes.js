const express = require("express");
const upload = require("../middlewares/uploadMiddleware");
const router = express.Router();
const attachmentController = require("../controllers/attachmentControllers");

router.post("/", upload.single("avatar"), attachmentController.create);

module.exports = router;
